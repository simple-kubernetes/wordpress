FROM wordpress:5.9.1

ENV HOME /var/www/html

WORKDIR $HOME

COPY diplom.conf /etc/apache2/sites-available/000-default.conf
COPY --chown=www-data:www-data Code $HOME

EXPOSE 80

CMD ["apache2-foreground"]